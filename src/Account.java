public class Account {
    private String id;
    private String name ;
    private int balance ;

    // khởi tạo đối tượng với đủ tham số
    public Account(String id, String name, int balance) {
        this.id = id;
        this.name = name;
        this.balance = balance;
    }

    // khởi tạo đối tượng với 2 tham số 
    public Account(String id, String name) {
        this.id = id;
        this.name = name;
    }


    public String getId() {
        return id;
    }


    public String getName() {
        return name;
    }

    public int getBalance() {
        return balance;
    }

    public int credit(int amount){
         this.balance += amount ;
         return this.balance;
    } 
    public int debit(int amount){
        if (amount <= balance) {
            this.balance -= amount ;
        }else System.out.println("Amount exceeded balance");
        return balance ;
    } 
    public int transfer(Account another , int amount){
        if (amount <= balance) {
            another.credit(amount);
            this.debit(amount);
        }else System.out.println("Amount exceeded balance");
        return balance ;
    } 


    @Override
    public String toString() {
        return "Account [balance=" + balance + ", id=" + id + ", name=" + name + "]";
    }


}
