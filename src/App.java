public class App {
    public static void main(String[] args) throws Exception {
        Account account1 = new Account("3", "Duong");
        Account account2 = new Account("5", "Linh", 1000);
        System.out.println("Tien ban dau");
        System.out.println(account1);
        System.out.println(account2);
        
        System.out.println("Tien khi duoc tang them");
       account2.credit(1000);
       System.out.println(account1);
        System.out.println(account2);

        System.out.println("Tien khi bi tru di");
       account2.debit(500);
       System.out.println(account1);
       System.out.println(account2);

       System.out.println("Tien khi chuyen tu tai khoan 2 sang tai khoan 1");
        account2.transfer(account1, 500);
       System.out.println(account1);
       System.out.println(account2);
    }
}
